var cacheName = 'cache-respaldo';
var appShellFiles = [
    'index.html',
    'css/styles.css',
    'images/1.png',
    'images/2.png',
    'images/3.png',
    'images/4.png',
    'images/5.png',
    'images/icons/pch192.png',
    'images/icons/pch256.png',
    'images/icons/pch512.png',
    'includes/conexion.php',
    'js/app.js',
    'js/notification.js',
    'pages/carrito.php',
    'pages/confirmar.php',
    'pages/dirrecion.php',
    'pages/enviopago.php',
    'pages/login.php',
    'pages/register.php',
    'manifest.json',
    'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
    'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js',
    'https://media.istockphoto.com/vectors/filled-shopping-cart-symbol-icon-vector-id1199519164?k=20&m=1199519164&s=170667a&w=0&h=cPuFb8wTwNazlnEwwDmlewGFJPRHKCWhW6DAn98hwYU=',
    'https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js',
    'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js'
];

var contentToCache = appShellFiles;

self.addEventListener("install", event => {
    console.log('[Service Worker] Instalando');
    event.waitUntil(
        caches.open(cacheName).then((cache) => {
            console.log('[Servicio Worker] Almacena todo en caché: contenido e intérprete de la aplicación');
            return cache.addAll(contentToCache);
        })
    );
});

self.addEventListener("activate", event => {
    event.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (key !== cacheName) {
                    return caches.delete(key);
                }
            }));
        })
    );
    console.log("[Servicio Worker] Activado");
});



self.addEventListener("fetch", function (event) {
    event.respondWith(
        caches.match(event.request).then((r) => {
            console.log('[Servicio Worker] Obteniendo recurso: ' + event.request.url);
            return r || fetch(event.request).then((response) => {
                return caches.open(cacheName).then((cache) => {
                    console.log('[Servicio Worker] Almacena el nuevo recurso: ' + event.request.url);
                    cache.put(event.request, response.clone());
                    return response;
                });
            });
        })
    );
});