<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="manifest" href="manifest.json">
  <link rel="icon" type="image/png" href="images/icons/pch192.png">
  <link rel="apple-touch-icon" href="images/icons/pch192.png">
  <link rel="apple-touch-icon" href="images/icons/pch256.png">
  <link rel="apple-touch-icon" href="images/icons/pch512.png">
  <title>PCHardware</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
</head>

<body>
  <!--Script Notifications-->
  <script src="js/notification.js"></script>
  <!--Script SW-->
  <script src="js/app.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">PCHardware</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Principal</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
              aria-expanded="false">
              Categorias
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="#">Tarjetas de Video</a></li>
              <li><a class="dropdown-item" href="#">Microprocesadores</a></li>
              <li><a class="dropdown-item" href="#">Memorias RAM</a></li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/login.php">Cuenta</a>
          </li>
          <div class="container">
            <a class="navbar-brand" href="pages/carrito.php">
              <img
                src="https://media.istockphoto.com/vectors/filled-shopping-cart-symbol-icon-vector-id1199519164?k=20&m=1199519164&s=170667a&w=0&h=cPuFb8wTwNazlnEwwDmlewGFJPRHKCWhW6DAn98hwYU="
                alt="" width="30" height="24">
            </a>
          </div>
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search">
          <button class="btn btn-outline-success" type="submit">Buscar</button>
        </form>
      </div>
    </div>
  </nav>

  <h1 class="text-center">Productos</h1>
  <hr>
  <div class="container-sm">
    <div class="row align-items-start">
      <div class="col">
        <div class="card" style="width: 18rem; height: 32rem;">
          <img src="images/3.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Memoria RAM Kingston Fury</h5>
            <p class="card-text">8GB, 3000MHz, Non-ECCm CL15, XMP</p>
            <p class="card-text text-end fw-bold">$800</p>
            <a href="#" class="btn btn-primary btn-long">Agregar</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card" style="width: 18rem; height: 32rem;">
          <img src="images/1.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Ryzen 7 2700</h5>
            <p class="card-text">S-AM4 3.20GHZ Eight Core</p>
            <p class="card-text text-end fw-bold">$2100</p>
            <a href="#" class="btn btn-primary btn-long">Agregar</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card" style="width: 18rem; height: 32rem;">
          <img src="images/2.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">MSI Geforce GTX 1050</h5>
            <p class="card-text">4GB 128-bit GDDR5, PCI Express x16 3.0</p>
            <p class="card-text text-end fw-bold">$2900</p>
            <a href="#" class="btn btn-primary btn-long">Agregar</a>
          </div>
        </div>
      </div>

    </div>
  </div>
  <hr>
  <div class="card text-center">
    <div class="card-body">
      <button type="button" onclick="notificar()" id="especial">Enviar Notificacion</button>
    </div>
  </div>
  <div class="card text-center">
    <div class="card-body">
      <h5 class="card-title">UTHermosillo</h5>
      <p class="card-text">Derechos Reservados © 2021 - 2021 </p>
    </div>
  </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
  integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
  integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</html>