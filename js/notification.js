Notification.requestPermission(status => {
    console.log("¿Quieres recibir las ultimas novedades que tenemos para ti?", status);
});

document.addEventListener("DOMContentLoaded", function () {
    if (!Notification) {
        alert("El navegador no soporta las notificaciones");
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notificar() {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    } else {
        var notification = new Notification("PCHardware",
            {
                icon: "images/icons/pch192.png",
                body: "Entra y descubre las nueva novedades que tenemos para ti!"
            }
        );

        notification.onclick = function(){
            window.open("http://PCHardware.net");
        }
    }
}