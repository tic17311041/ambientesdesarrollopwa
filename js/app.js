if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js', { scope: '/index.php' }).then(function (registration) {
        console.log('[Servicio Worker] registration completado:', registration);
    }).catch(function (error) {
        console.log('[Servicio Worker] registro fallido:', error);
    });
} else {
    console.log('[Servicio Worker] No soporta este ordenador');
}