<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="manifest" href="../manifest.json">
    <link rel="icon" type="image/png" href="../images/icons/pch192.png">
    <link rel="apple-touch-icon" href="images/icons/pch192.png">
    <link rel="apple-touch-icon" href="images/icons/pch256.png">
    <link rel="apple-touch-icon" href="images/icons/pch512.png">
    <title>PCHardware</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/styles.css">
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php/">PCHardware</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="../index.php">Principal</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Categorias
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Tarjetas de Video</a></li>
                            <li><a class="dropdown-item" href="#">Microprocesadores</a></li>
                            <li><a class="dropdown-item" href="#">Memorias RAM</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="login.php">Cuenta</a>
                    </li>
                    <div class="container">
                        <a class="navbar-brand" href="#">
                            <img src="https://media.istockphoto.com/vectors/filled-shopping-cart-symbol-icon-vector-id1199519164?k=20&m=1199519164&s=170667a&w=0&h=cPuFb8wTwNazlnEwwDmlewGFJPRHKCWhW6DAn98hwYU=" alt="" width="30" height="24">
                        </a>
                    </div>
                </ul>
                <form class="d-flex">
                    <input class="form-control me-2" type="search">
                    <button class="btn btn-outline-success" type="submit">Buscar</button>
                </form>
            </div>
        </div>
    </nav>

    <h1 class="text-center">Carrito Envio y Pago</h1>
    <hr>
    <div class="container-sm">
        <div class="container">

            <div class="row align-items-start">
                <div class="col text-center">
                    Productos
                </div>
                <div class="col text-center">
                    Direccion
                </div>
                <div class="col text-center">
                    Envio y Pago
                </div>
                <div class="col text-center">
                    Confirmar Pedido
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr>
            <div class="row align-items-start">
                <div class="col text-center">
                    <img src="../images/4.png" style="width: 100px; height: 50px;" alt="" srcset="">
                    <input type="checkbox" name="" id="" checked>DHL Express
                </div>
                <div class="col text-center">
                    <img src="../images/5.png" style="width: 100px;" alt="" srcset="">
                    <input type="checkbox" name="" id="" checked> Pago por Oxxo
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <a href="confirmar.php"><button class="btn btn-primary" type="button">Siguiente</button></a>
                </div>
            </div>
            <hr>
            <div class="card text-center">
                <div class="card-body">
                    <h5 class="card-title">UTHermosillo</h5>
                    <p class="card-text">Derechos Reservados © 2021 - 2021 </p>
                </div>
            </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</html>